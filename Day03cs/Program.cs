﻿var input = File.ReadAllLines("./input");


var parsed = input
	.SelectMany(line => line.Select((v, i) => new { v, i }))
	.GroupBy(x => x.i, x => x.v)
	.Select(g => g.ToList());

var gammaStr = parsed
	.Select(x => x.Count(y => y == '1') > x.Count / 2 ? '1' : '0')
	.ToArray();

var gammaRate = Convert.ToInt32(new string(gammaStr), 2);

var epsilonStr = parsed
	.Select(x => x.Count(y => y == '1') > x.Count / 2 ? '0' : '1')
	.ToArray();

var epsilonRate = Convert.ToInt32(new string(epsilonStr), 2);

Console.WriteLine(gammaRate * epsilonRate);