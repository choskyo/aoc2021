﻿open System.IO

let input = File.ReadAllLines("input")

type Point = (int * int)

type Line = { p1: Point; p2: Point }

let parseCoord (raw: string) : Line =
    let noArrow = raw.Replace(" -> ", ",")

    let x = noArrow.Split(',') |> Array.map int

    { p1 = (x.[0], x.[1])
      p2 = (x.[2], x.[3]) }

let lines = input |> Array.map parseCoord

let nonDiagLines =
    lines
    |> Array.filter (fun { p1 = (x1, y1); p2 = (x2, y2) } -> x1 = x2 || y1 = y2)

let render { p1 = (x1, y1); p2 = (x2, y2) } =
    let [| lx_min; lx_max |] = [| x1; x2 |] |> Array.sort
    let [| ly_min; ly_max |] = [| y1; y2 |] |> Array.sort

    if x1 = x2 || y1 = y2 then
        [| for x in lx_min .. lx_max do
               for y in ly_min .. ly_max -> (x, y) |]
    else
        let len = lx_max - lx_min
        let mod_x n i = if x1 > x2 then (n - i) else (n + i)
        let mod_y n i = if y1 > y2 then (n - i) else (n + i)
        [| for i in 0 .. len -> (mod_x x1 i, mod_y y1 i) |]

let solve inp =
    inp
    |> Array.collect render
    |> Array.countBy id
    |> Array.filter (fun (_, c) -> c >= 2)
    |> Array.length

let getPartOne () = solve nonDiagLines

let getPartTwo () = solve lines

printfn "%d" (getPartOne ())
printfn "%d" (getPartTwo ())
