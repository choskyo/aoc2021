﻿open System.IO
open System

let file = "input"

let input =
    File.ReadAllLines file
    |> Array.map (fun x ->
        (((x.Split " | ").[0].Split ' ')
         |> Array.map (fun s -> s.ToCharArray())),
        ((x.Split " | ").[1].Split ' ')
        |> Array.map (fun s -> s.ToCharArray()))

let p1Segs = [| 1; 4; 7; 8 |]

let getSegMatch (segs: char []) (knownSegs: char []) =
    knownSegs
    |> Array.forall (fun c -> Array.contains c segs)

let getSegDiff (segs: char []) (knownSegs: char []) =
    knownSegs
    |> Array.filter (fun x -> Array.contains x segs = false)
    |> Array.length

let matchComplexDigits (segments: char []) (knownSegs: char [] []) =
    Array.findIndex
        (fun known ->
            (getSegMatch known segments)
            && (getSegMatch segments known))
        knownSegs

let matchDigits (segments: char []) (knownSegs: char [] []) =
    match segments.Length with
    | 2 -> 1
    | 4 -> 4
    | 3 -> 7
    | 7 -> 8
    | _ when knownSegs.Length > 0 -> matchComplexDigits segments knownSegs
    | _ -> -1

let partOneRes =
    input
    |> Array.map (fun (_, x) -> x)
    |> Array.collect id
    |> Array.filter (fun x -> Array.contains (matchDigits x [||]) p1Segs)
    |> Array.length

let getKnownSegs (segs: char []) =
    let i = matchDigits segs [||]
    (i, segs)

let getRowSegmentSets (inputs: char [] []) =
    let easySegs =
        inputs
        |> Array.map (fun x -> getKnownSegs x)
        |> Array.filter (fun (i, _) -> i >= 0)
        |> Array.sortBy (fun (i, _) -> i)
        |> Array.map (fun (_, arr) -> arr)

    let threeSegs =
        inputs
        |> Array.find (fun x -> x.Length = 5 && getSegMatch x easySegs.[0])

    let sixSegs =
        inputs
        |> Array.find (fun x -> x.Length = 6 && getSegDiff x easySegs.[0] = 1)

    let nineSegs =
        inputs
        |> Array.find (fun x -> x.Length = 6 && getSegMatch x threeSegs)

    let trSeg =
        Array.filter (fun c -> Array.contains c sixSegs = false) easySegs.[0]
        |> Array.exactlyOne

    let brSeg =
        Array.filter (fun c -> c <> trSeg) easySegs.[0]
        |> Array.exactlyOne

    let twoSegs =
        inputs
        |> Array.find (fun x -> x.Length = 5 && Array.contains brSeg x = false)

    let fiveSegs =
        inputs
        |> Array.find (fun x -> x.Length = 5 && Array.contains trSeg x = false)

    let knownSoFar =
        [| easySegs.[0]
           twoSegs
           threeSegs
           easySegs.[1]
           fiveSegs
           sixSegs
           easySegs.[2]
           easySegs.[3]
           nineSegs |]

    let zeroSigs =
        inputs
        |> Array.find (fun x ->
            x.Length = 6
            && Array.contains x knownSoFar = false)

    Array.append [| zeroSigs |] knownSoFar

let solveRow ((inp, out): (char [] [] * char [] [])) : int =
    let segSets = getRowSegmentSets inp

    let rowDigits =
        out
        |> Array.map (fun x -> matchDigits x segSets)
        |> Array.map string

    ("", rowDigits) |> String.Join |> int

let partTwoRes =
    input
    |> Array.map (fun (row) -> solveRow row)
    |> Array.sum

printfn "wah"
