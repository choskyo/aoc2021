﻿open System.IO

type Sub =
    { horizontal: int
      depth: int
      aim: int }

type Dir =
    | Forward
    | Up
    | Down
    | None

let getRes sub = sub.horizontal * sub.depth

type Cmd = { dir: Dir; mag: int }

let parseDir dir =
    match dir with
    | "forward" -> Forward
    | "up" -> Up
    | "down" -> Down
    | _ -> None

let lines =
    File.ReadAllLines "input"
    |> Array.map (fun x -> x.Split " ")
    |> Array.map (fun x ->
        { dir = parseDir x.[0]
          mag = int x.[1] })

let p1Folder (sub: Sub) (cmd: Cmd) =
    match cmd with
    | { dir = Forward; mag = mag } -> { sub with horizontal = sub.horizontal + mag }
    | { dir = Down; mag = mag } -> { sub with depth = sub.depth + mag }
    | { dir = Up; mag = mag } -> { sub with depth = sub.depth - mag }
    | _ -> sub // shut up compiler warning

let p2Folder (sub: Sub) (cmd: Cmd) =
    match cmd with
    | { dir = Forward; mag = mag } ->
        { sub with
            horizontal = sub.horizontal + mag
            depth = sub.depth + mag * sub.aim }
    | { dir = Down; mag = mag } -> { sub with aim = sub.aim + mag }
    | { dir = Up; mag = mag } -> { sub with aim = sub.aim - mag }
    | _ -> sub // shut up compiler warning

Array.fold p1Folder { horizontal = 0; depth = 0; aim = 0 } lines
|> getRes
|> printfn "p1: %d"

Array.fold p2Folder { horizontal = 0; aim = 0; depth = 0 } lines
|> getRes
|> printfn "p2: %d"
