﻿open System.IO

let input = File.ReadAllLines "input"

let lot = input.[0]

let parsedLot = lot.Split(',')

type Num = { num: string; selected: bool }

type Board = { nums: Num [] [] }

let mapRows (row: string) =
    row.Split(' ')
    |> Array.except [ "" ]
    |> Array.map (fun r -> { num = r; selected = false })

let boards =
    input.[1..]
    |> Array.except [ "" ]
    |> Array.map mapRows
    |> Array.chunkBySize 5
    |> Array.map (fun rowGroup -> { nums = rowGroup })

let updateRow (row: Num []) (draw: string) : Num [] =
    row
    |> Array.map (fun x ->
        if x.num = draw then
            { num = x.num; selected = true }
        else
            x)

let hasRowWon (row: Num []) : bool =
    row
    |> Array.filter (fun x -> x.selected = false)
    |> Array.length = 0

let hasColWon (nums: Num [] []) : bool =
    nums
    |> Array.transpose
    |> Array.map (fun col -> hasRowWon col)
    |> Array.filter (fun res -> res)
    |> Array.length > 0

let hasBoardWon (board: Board) : bool =
    let winningRows =
        board.nums
        |> Array.filter (fun x -> (hasRowWon x))
        |> Array.length

    let anyWinningCols = hasColWon board.nums

    winningRows > 0 || anyWinningCols

let updateBoard (draw: string) (board: Board) : Board =
    let newNums =
        board.nums
        |> Array.map (fun row -> updateRow row draw)

    { nums = newNums }


let rec getBoardResultAndFinalState (turn: int) (board: Board) : (int * int * Board) =
    let draw = parsedLot.[turn]
    let newBoard = updateBoard draw board

    if hasBoardWon newBoard then
        (draw |> int, turn - 1, newBoard)
    else if turn = (Array.length parsedLot - 1) then
        (draw |> int, -1, newBoard)
    else
        getBoardResultAndFinalState (turn + 1) newBoard

let getWinningBoardScore (board: Board) (draw: int) : int =
    let unmarkedSum =
        board.nums
        |> Array.reduce Array.append
        |> Array.filter (fun x -> x.selected = false)
        |> Array.map (fun x -> x.num |> int)
        |> Array.sum

    unmarkedSum * draw

let boardResults =
    boards
    |> Array.map (fun x -> getBoardResultAndFinalState 0 x)

let (finalDraw, drawCount, finalState) =
    boardResults
    |> Array.minBy (fun (_, drawCount, _) -> drawCount)

printfn "%d" (getWinningBoardScore finalState finalDraw)

let (p2fd, p2dc, p2fs) =
    boardResults |> Array.maxBy (fun (_, dc, _) -> dc)

printfn "%d" (getWinningBoardScore p2fs p2fd)
