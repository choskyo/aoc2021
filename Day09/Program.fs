﻿open System.IO
open Microsoft.FSharp.Collections

type Point = { x: int; y: int; v: int }

let input =
    File.ReadAllLines "input_ex"
    |> Array.mapi (fun y row -> (y, (row.ToCharArray())))
    |> Array.map (fun (y, row) -> (Array.mapi (fun x c -> { x = x; y = y; v = c |> string |> int }) row))
    |> Array.reduce Array.append
    |> Seq.map (fun p -> (p.y, p.x), p.v)
    |> Map.ofSeq

let tryFindPoint y x = input |> Map.tryFind (y, x)

let dirs = [ (0, 1); (0, -1); (1, 0); (-1, 0) ]

let flipB b = if b then false else true

let isLowPoint y x v =
    dirs
    |> List.map (fun (dY, dX) ->
        let adj = tryFindPoint (y + dY) (x + dX)

        match adj.IsSome with
        | true -> adj.Value > v
        | false -> true)
    |> List.exists (fun isLowerThan -> isLowerThan = false)
    |> flipB

let getRiskValue v = v + 1

let lowPoints =
    input
    |> Map.filter (fun (y, x) v -> isLowPoint y x v)

let p1Res =
    lowPoints
    |> Map.map (fun _ v -> getRiskValue v)
    |> Map.toList
    |> List.map (fun (_, v) -> v)
    |> List.sum

printfn "wah"
