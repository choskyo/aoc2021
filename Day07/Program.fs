﻿open System.IO
open System

let (?) truth trueBranch falseBranch =
    if truth then
        trueBranch
    else
        falseBranch

let toArray (str: string) = str.Split(',')

let input =
    File.ReadAllLines "input_ex"
    |> Array.exactlyOne
    |> toArray
    |> Array.map int
    |> Array.sort
    |> Array.groupBy id
    |> Array.map (fun (pos, arr) -> (pos, arr.Length))

let getFuelCost target ((pos, count): (int * int)) = Math.Abs(pos - target) * count

let getPartTwoFuelCost target ((pos, count): (int * int)) =
    let dist = Math.Abs(pos - target)
    (dist * (dist + 1) / 2) * count

let (minPos, _) = input.[0]
let (maxPos, _) = input.[input.Length - 1]

let rec getRes (score: int) (targetPos: int) (isPartTwo) : int =
    if targetPos = maxPos then
        score
    else
        let newScore =
            input
            |> Array.sumBy (fun x -> isPartTwo ? (getPartTwoFuelCost targetPos x) (getFuelCost targetPos x))
            |> min score

        getRes newScore (targetPos + 1) isPartTwo

let partOne = getRes Int32.MaxValue minPos false

let partTwo = getRes Int32.MaxValue minPos true

printfn "wah"
