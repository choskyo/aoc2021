﻿open System.IO

let input =
    File.ReadAllLines("input").[0].Split(',')
    |> Array.map int
    |> Array.groupBy id
    |> Array.map (fun (c, a) -> (c, a.Length |> int64))

let rec processFish (fish: (int * int64) []) (day: int) (maxDay: int) : (int * int64) [] =
    if day = maxDay then
        fish
    else
        let fishToAdd =
            fish
            |> Array.filter (fun (t, _) -> t = 0)
            |> Array.map (fun (_, c) -> c)
            |> Array.tryExactlyOne
            |> Option.fold (fun _ v -> v) 0

        let tempProcessed =
            fish
            |> Array.map (fun (t, c) -> (t - 1, c))
            |> Array.map (fun (t, c) ->
                if t <> 6 then
                    (t, c)
                else
                    (6, c + fishToAdd))

        let res =
            if Array.exists (fun (t, _) -> t = 6) tempProcessed then
                Array.append tempProcessed [| (8, fishToAdd) |]
            else
                Array.append tempProcessed [| (6, fishToAdd); (8, fishToAdd) |]
            |> Array.filter (fun (t, c) -> t >= 0 && c > 0)

        processFish res (day + 1) maxDay

let res = processFish input 0 256

printfn
    "%d"
    (res
     |> Array.map (fun (_, c) -> c)
     |> Array.reduce (+))
