﻿open System.IO
open Microsoft.FSharp.Collections

let lines =
    File.ReadAllLines "./input_p1" |> Array.map int

let partOne =
    Array.pairwise lines
    |> Array.filter (fun (a, b) -> a < b)
    |> Array.length

let partTwo =
    Array.windowed 3 lines
    |> Array.map Array.sum
    |> Array.pairwise
    |> Array.filter (fun (a, b) -> a < b)
    |> Array.length

printfn "%d" partOne
printfn "%d" partTwo
