module PartTwo

open System
open Shared

let solution (input: string []) : int =
    let parsed = input |> mapToCharCount

    let getRate (leastCommon: bool) =
        let initCommon = getCommon parsed leastCommon

        let rec solve (nums: string []) (mostCommonBit: char) (i: int) : string =
            let newNums =
                nums
                |> Array.filter (fun x -> x.[i] = mostCommonBit)

            let newCommon =
                getCommon (newNums |> mapToCharCount) leastCommon

            if newNums.Length = 1 then
                newNums.[0]
            else
                solve newNums (newCommon.[i + 1]) (i + 1)

        let resBinary = solve input initCommon.[0] 0

        Convert.ToInt32(resBinary, 2)

    let oxyRate = getRate false
    let co2Rate = getRate true

    (oxyRate * co2Rate)
