module Shared

let toCharArray (str: string) = str.ToCharArray()

let mapToCharCount input =
    input
    |> Array.map toCharArray
    |> Array.transpose
    |> Array.map (Array.countBy id)

let getMostCommon (arr: (char * int) []) (tieBreaker: int) =
    if arr.Length = 1 then
        let (c, _) = arr.[0]
        c |> string |> int
    else
        let (char0, count0) = arr.[0]
        let (_, count1) = arr.[1]

        if count0 = count1 then
            tieBreaker
        else if char0 = '0' then
            if count0 > count1 then 0 else 1
        else if count0 > count1 then
            1
        else
            0

let flipMapper n = if n = '1' then '0' else '1'

let getCommon vals flip =
    vals
    |> Array.map (fun x -> getMostCommon x 1)
    |> Array.map string
    |> Array.map char
    |> Array.map (fun x -> if flip then flipMapper x else x)
