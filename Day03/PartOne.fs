module PartOne

open System
open Shared

let solution (input: string []) : int =
    let parsed = input |> mapToCharCount

    let epsilonStr =
        getCommon parsed false
        |> Array.map string
        |> String.concat ""

    let gammaStr =
        epsilonStr
        |> toCharArray
        |> Array.map flipMapper
        |> Array.map string
        |> String.concat ""

    let epsilon = Convert.ToInt32(epsilonStr, 2)
    let gamma = Convert.ToInt32(gammaStr, 2)

    (epsilon * gamma)
