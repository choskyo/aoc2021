﻿open System.IO

let input = File.ReadAllLines "input_ex"

input |> PartOne.solution |> printfn "one: %d"

input |> PartTwo.solution |> printfn "two: %d"
